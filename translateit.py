#! /usr/bin/python
# -*- coding: utf-8 -*-
#Translates text from clipboard using the default webbrowser
#Depedencies: PyGtk, PyZenity, Python, A Web Browser, Internet Connection.
#
# Copyright 2012 Stef Kariotidis <stef@iccode.net>
#
# This file is part of translateit.py script.
#
# translateit.py script is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# translateit.py script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with translateit.py script.  If not, see <http://www.gnu.org/licenses/>.

import webbrowser
from gi.repository import Gtk, Gdk
from PyZenity import InfoMessage as msg

#set target language
translate_to = "el"

#get reference to the clipboard
clipboard = Gtk.Clipboard.get(Gdk.SELECTION_PRIMARY)
#wait for text
text = clipboard.wait_for_text()

#check if we have a selection and try to google it
if text:
	try:
		webbrowser.open_new_tab("http://translate.google.com/#auto|%s|%s" % (translate_to, text))
	except  webbrowser.Error:
		msg("Could not make the translate!")
