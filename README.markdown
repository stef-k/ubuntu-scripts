ubuntu-scripts
==============

Some scripts mainly in Python trying to make life easier in Ubuntu Linux

# Dependencies:
* Python (which is already installed)
* PyGtk (which is probably already installed)
* Zenity (as above)
* PyZenity Python-Zenity wrapper (use apt to install it)

# Scripts:

* googleit.py: search with Google
* translateit.py: translate with Google

# Recommended usage:

1. Make script executable, i.e: chmod +x googleit.py
2. Create a new custom shortcut and assign the scripts using **absolute paths**.

## googleit.py 

Opens a new browser tab and starts a Google search using selected text from almost anywhere (uses Gtk clipboard).

## translateit.py

Open a new browser tab and tries to translate the selected text using Google translate, from auto recognized language to a specified language in the script.
