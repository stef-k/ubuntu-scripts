#! /usr/bin/python
# -*- coding: utf-8 -*-
#Makes a Google search using text from clipboard and default webbrowser
#Depedencies: PyGtk, PyZenity, Python, A Web Browser, Internet Connection.
#
# Copyright 2012 Stef Kariotidis <stef@iccode.net>
#
# This file is part of googleit.py script.
#
# googleit.py script is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# googleit.py script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with googleit.py script.  If not, see <http://www.gnu.org/licenses/>.

import webbrowser
from gi.repository import Gtk, Gdk
from PyZenity import InfoMessage as msg

#get reference to the clipboard
clipboard = Gtk.Clipboard.get(Gdk.SELECTION_PRIMARY)
#wait for text
text = clipboard.wait_for_text()

#check if we have a selection and try to google it
if text:
	try:
		webbrowser.open_new_tab("http://www.google.com/search?q=%s" %(text))
	except  webbrowser.Error:
		msg("Could not make a search!")
